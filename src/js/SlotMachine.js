import Reel from "./Reel";
import WinLabelAnimate from './WinLabelAnimate';

class SlotMachine extends PIXI.Container
{
    constructor()
    {
        super();

        this.TIME_BETWEN_STOP_REEL = 250;

        this.isRunning = false;
        this.reels = [new Reel(this), new Reel(this), new Reel(this), new Reel(this), new Reel(this)];

        this.winAnimate = new WinLabelAnimate();
    }
    
    init(elementList, slotData)
    {
        this.elementList = elementList;
        this.slotData = slotData;

        for(let i=0; i<this.reels.length; i++)
        {
            this.reels[i].init(this.elementList);
            this.reels[i].position.set( 90 + i*180, 85);
            this.addChild(this.reels[i]);
        }

        this.winAnimate.position.set(450, 290);
        this.addChild(this.winAnimate);
    }

    create()
    {
        for(let i=0; i<this.reels.length; i++)
        {
            this.reels[i].create();
        }

        let mask = new PIXI.Graphics();
        mask.beginFill(0xff0000, 0.4);
        mask.drawRect(0, 0, 900, 570);
        this.mask = mask;
        this.addChild(mask);

    }

    start()
    {
        for(let i=0; i<this.reels.length; i++)
        {
            this.reels[i].start();
        }
        this.onStartRotate();
    }

    stop()
    {
        let board = this.slotData.getBoard(); 
         
        return new Promise((resolve, rejects)=>{
            let reels = this.reels;
            let currReel = reels[0];
    
            let reelPromise = currReel.stopping(board[0]);
     
            for(let i=1; i<reels.length; i++)
            {
                setTimeout(()=>
                {
                    
                    reelPromise =  reelPromise.then(result=> reels[i].stopping(board[i]));
                    if(i===reels.length-1) 
                    {
                        reelPromise.then(result=> reels[i].stop(board[i])).then(()=>{
                            resolve(); 
                            this.onStopRotate();
                        });
                    }

                }, i*this.TIME_BETWEN_STOP_REEL);
               
            }
        })
    }

    onStartRotate()
    { 
        this.isRunning = true;
    }

    onStopRotate()
    { 
        this.isRunning = false; 

        if(this.slotData.win > 0)
        {
            this.winAnimate.start(this.slotData.win);

            let lines = this.slotData.winLines;
            
            lines.forEach((el)=>{
                el.elementsPos.forEach((pos)=>{ 
                    let element = this.getElementAt(pos[0], pos[1]);
                    element.setData('isWin', 1);
                })
            });

            this.reels.forEach(reel => reel.startAnime());
        }

    }

    update(delta)
    {
        for(let i=0; i<this.reels.length; i++)
        {
            this.reels[i].update(delta);
        }

        this.winAnimate.update(delta);
    }

    getElementAt(x, y)
    {
        return this.reels[x].getElementAt(y);
    }
    
    getRandomElementTexture()
    {
        return this.elementList[Math.floor(Math.random()*this.elementList.length)];
    }

    get isAnimateSlot()
    {
        let res = false;
        for(let i=0; i<this.slotData.COUNT_COL; i++)
        {
            for(let j=0; j<this.slotData.COUNT_ROW; j++)
            {
                if(this.getElementAt(i, j).isAnimate)
                    return true;
            }   
        }

        return false || this.winAnimate.isAnimate;
    }

}

export default SlotMachine;