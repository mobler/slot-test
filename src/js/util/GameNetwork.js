
import Network from './Network';

var GameNetworkInstance;

class GameNetwork
{
    constructor()
    {
        this.API_URL = 'http://mykyta.kyiv.ua/gs/';
    }

    getSlotSpin({bet} = {bet: 1})
    { 

        return new Promise((resolve, reject)=>
        {
            Network.httpGet({url:  `${this.API_URL}?action=bet&bet=${bet}`})
                   .then(
                        result =>
                        {
                            if(resolve)
                            {
                                // симулируем ошибку для проверки повторных запросов
                                // if(Math.random() > 0.7)
                                //     reject('test error');
                                // 
                                    
                                try
                                {
                                    let _json = JSON.parse(result);
                                    
                                    if(resolve)
                                        resolve(_json);
                                }catch(error)
                                {
                                    if(reject)
                                        reject(error);
                                }
                                
                            }
                        },
                        error =>
                        {
                            if(reject)
                                reject(error);
                        });
        });
    }
 
}

if (!GameNetworkInstance) {
    GameNetworkInstance = new GameNetwork();
}
  
export default GameNetworkInstance;