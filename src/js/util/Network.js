
var networkInstance;

class Network
{
    constructor()
    {
        this.xhr = new XMLHttpRequest();
    }

    httpGet(config)
    {
        let xhr = this.xhr;

        return new Promise((resolve, reject)=>
        {

            xhr.onreadystatechange = function() 
            {
                if (xhr.readyState != 4) return;
                
                if (xhr.status != 200) 
                {
                    if(reject)
                    reject(xhr.status + ': ' + xhr.statusText);
                 
                } else 
                {
                    if(resolve)
                        resolve(xhr.responseText);
                  
                }
              
              }
               
            
            xhr.open("GET", config.url, true);
            xhr.send();
        });
    }
}

if (!networkInstance) {
    networkInstance = new Network();
}
  
export default networkInstance;