import {RES} from './const';

class WinLabelAnimate extends PIXI.Container
{
    constructor(time)
    {
        super();

        this.time = 1200;
        this.timer = 0;
        this.progress = 0;
        this.isAnimate = false;
        this.toMoney = 0;
        this.currentMoney = 0;

        this.label = new PIXI.Text('', {fontFamily : 'Arial', fontWeight: 'bold', fontSize: 120, fill : 0xffffff, align : 'center', 
        strokeThickness: true, stroke: 0xffb600, strokeThickness: 10});
        this.label.anchor.set(0.5);
        this.addChild(this.label);
        
        this.visible = false;
    }

    start(to)
    {
        this.progress = 0;
        this.timer = 0;
        this.isAnimate = true;
        this.visible = true;

        this.toMoney = to;
        PIXI.sound.play(RES.SOUND.WIN_LABEL);
    }

    stop()
    {
        this.isAnimate = false;
        this.visible = false;
    }

    update(delta)
    {
        if(this.isAnimate)
        {
            this.timer += delta;
            this.progress = this.timer / this.time;

            this.scale.set(1 + this.progress* 0.5);
            this.label.text = (this.toMoney * Math.min(this.progress*2.5, 1)).toFixed(0);

            if(this.progress >= 1)
            {
                this.progress = 1;
                this.stop();
            }
        }
        
    }


    
}

export default WinLabelAnimate;