import * as PIXI from 'pixi.js';
import * as SOUND from 'pixi-sound';
 
import {WINDOW_W, WINDOW_H, RES} from './const';
import Game from './Game';

var app = new PIXI.Application(WINDOW_W, WINDOW_H, 
{
    view: document.getElementById('game'),
    resolution: window.resolution
});

var game = new Game();
game.init();
game.create();

var renderer = app.renderer;
let globalContainer = new PIXI.Container(); 

globalContainer.addChild(game);

app.stage.addChild(globalContainer);  
  
app.ticker.add(function() 
{
    let delta = app.ticker.elapsedMS;
    game.update(delta);
});

window.onresize = function(ev)
{
    onResize();
}

function onResize()
{
    let _h = document.body.clientHeight;
    let _w = document.body.clientWidth;

    var _scaleH =  _h / WINDOW_H;
    var _scaleW =  _w / WINDOW_W;
    var _scale = Math.min(_scaleW, _scaleH);

    
    globalContainer.scale.set(_scale);
    
    renderer.resize(WINDOW_W * _scale, WINDOW_H * _scale);

    if(_scaleH < _scaleW)
        renderer.view.style.marginLeft = `${_w/2 - WINDOW_W * _scale / 2}px`;
    else
        renderer.view.style.marginTop = `${_h/2 - WINDOW_H * _scale / 2}px`;
} 

onResize();
_addSounds();

function _addSounds()
{
    PIXI.sound.add(RES.SOUND.REEL_STOP,  RES.SOUND.REEL_STOP);
    PIXI.sound.add(RES.SOUND.WIN_LABEL,  RES.SOUND.WIN_LABEL);
}