class ElementAnimateNoWin
{
    constructor(element)
    {
        this.element = element;

        this.timer = 0;
        this.time = 1200;
        this.progress = 0;
    }
 
    update(delta)
    {
        this.timer += delta;
        this.progress = this.timer / this.time ; 
    }

    beforeAnimate()
    {
        this.element.spr.tint = 0xffffff;
    }

    updateProgress()
    {
        this.element.spr.tint = 0x888888;
    }

    afterAnimate()
    {
        this.element.spr.tint = 0xffffff;
    }
 
}

export default ElementAnimateNoWin;