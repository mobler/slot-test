 
export const WINDOW_W = 1280;
export const WINDOW_H = 720; 

export const RES = 
{
    SPRITE:
    {
        ICON1: './res/img/1.png',
        ICON2: './res/img/2.png',
        ICON3: './res/img/3.png',
        ICON4: './res/img/4.png',
        ICON5: './res/img/5.png',
        ICON6: './res/img/6.png',
        ICON7: './res/img/7.png',
        ICON8: './res/img/8.png',
        ICON9: './res/img/9.png',
        ICON10: './res/img/10.png',

        BACKGROUND: './res/img/background.png',
        BTN_MINUS: './res/img/btn_minus.png',
        BTN_PLUS: './res/img/btn_plus.png',
        BTN_START: './res/img/btn_start.png'
    },

    SOUND:{
        REEL_STOP: './res/sound/reelstop.mp3',
        WIN_LABEL: './res/sound/bell.mp3'
    }
}