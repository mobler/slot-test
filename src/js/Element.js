import ElementAnimateWin from "./ElementAnimateWin";
import ElementAnimateNoWin from "./ElementAnimateNoWin";

class Element extends PIXI.Container
{
    constructor(texture)
    {
        super();
        this.data = {
            isWin: -1 //Участвует в комбинации, -1 - сброс, 0 - не участвует, 1-участвует
        }
        let spr = new PIXI.Sprite(texture);
        spr.anchor.set(0.5);
        spr.scale.set(0.8)
        this.addChild(spr); 

        this.spr = spr; 
        this.isAnimate = false;
        
        this.animate = null;
    }

    setData(key, value)
    {
        this.data[key] = value;
    }

    onStartRotate()
    {
       this.data.isWin = -1;
       this.isAnimate = false;
       this.spr.tint = 0xffffff;
    }

    onStopRotate()
    {
        this.data.isWin = 0; 
    }

    update(delta)
    {
        if(this.isAnimate)
        {
            this.animate.update(delta);
            
            if(this.animate.progress >= 1)
            {
                this.onStopAnimate();
                return;
            }
            this.animate.updateProgress(this.progress);
            
        }
    }

   

    onStartAnimate()
    {  
        if(this.data.isWin === 1)
            this.animate = new ElementAnimateWin(this);
        else
            this.animate = new ElementAnimateNoWin(this);

        this.isAnimate = true;
        this.animate.beforeAnimate();
    }

    onStopAnimate()
    {
        this.isAnimate = false;
        this.animate.afterAnimate();
    }
 
}

export default Element;