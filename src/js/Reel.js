import Element from './Element';
import {RES} from './const';

class Reel extends PIXI.Container
{
    constructor(slotMachine)
    {
        super();
        
        this.slotMachine = slotMachine;

        this.status = Reel.STATUS_STOP;
        
        this.stageElements = new PIXI.Container();
        
        this.SPEED_ROTATE = 4.0;
        this.MIN_COUNT_ROTATE_ELEMENT = 10; // минимальное количество прокрученных элементов
        this.START_POS_Y_ELEMENT = -200;
        this.BETWEEN_Y_ELEMENT = 200;
        
        this.COUNT_ADD_ELEMENT = 2;
        this.COUNT_VIEW_ELEMENTS = 3;
        this.COUNT_ELEMENTS = this.COUNT_ADD_ELEMENT + this.COUNT_VIEW_ELEMENTS;

        this.timer =0;
        this.valueDrop = null; // Что выпало
        this.isCanStop = false;
        this.indexInsertDrop = 0; 
        
        this.MIN_POS_Y_ELEMENT = this.COUNT_ELEMENTS * this.BETWEEN_Y_ELEMENT + this.START_POS_Y_ELEMENT;

        this.addChild(this.stageElements);
    }

    static STATUS_RUN(){return 1};
    static STATUS_STOP(){return 2};
    static STATUS_NEED_STOP(){return 3};
    
    init(elementList)
    {
        this.elementList = elementList;
    }
    
    create()
    {
        
        for(let i=0; i<this.COUNT_ELEMENTS; i++)
        {
            this.addNextElement(i * this.BETWEEN_Y_ELEMENT + this.START_POS_Y_ELEMENT); 
        }
    }

    start()
    { 
        this.status = Reel.STATUS_RUN;
        this.MIN_COUNT_ROTATE_ELEMENT = 1; 
        this.indexInsertDrop = -1;
        this.valueDrop = [];
        this.isCanStop = false;
        this.timer =0;

        this.onStart();
    }
    
    // Promise resolve  когда начал останавливаться  
    stopping(values)
    {
        this.valueDrop = values;
        this.isCanStop = true;

        this.stopPromise = new Promise ((resolve, reject)=>{
             
            let promiseHandler = ()=>
            { 
                if(this.status === Reel.STATUS_NEED_STOP)
                    resolve();
                else    
                    setTimeout(promiseHandler, 50);
            }

           promiseHandler();

        });

        return this.stopPromise;
    }

    stop(value)
    {
        
     return new Promise ((resolve, reject)=>{
             
            let promiseHandler = ()=>
            { 
                if(this.status === Reel.STATUS_STOP)
                {
                    resolve();
                    
                }
                else    
                    setTimeout(promiseHandler, 50);
            }

           promiseHandler();

        });
 
    }

    update(delta)
    {

        if(this.status === Reel.STATUS_RUN || this.status === Reel.STATUS_NEED_STOP)
        {
            this.timer += delta;
            
            let velocity = this.SPEED_ROTATE * (delta%this.BETWEEN_Y_ELEMENT);
            let bElement = this._findBottomElement();

            if(bElement.y + velocity > this.MIN_POS_Y_ELEMENT)
            {
                let lastElementY = bElement.y;
                this.stageElements.removeChild(bElement);
                
                this.addNextElement(this.START_POS_Y_ELEMENT + (lastElementY - this.MIN_POS_Y_ELEMENT));  

                this.MIN_COUNT_ROTATE_ELEMENT--;
                if(this.MIN_COUNT_ROTATE_ELEMENT<=0 && this.isCanStop)
                {
                    this.status = Reel.STATUS_NEED_STOP;
                    
                    if(this.indexInsertDrop+1 > this.COUNT_ELEMENTS-1)
                    {
                        this.status = Reel.STATUS_STOP;
                        this.onStop();
                    }

                    velocity = this.MIN_POS_Y_ELEMENT - lastElementY;

                    this.indexInsertDrop++;
                }

                
                
              
            }
            
            let reelList = this.stageElements.children;
            for (let i = 0; i <reelList.length; i++) 
            {
                reelList[i].y += velocity; 
            }
        } 
        
      
        let reelList = this.stageElements.children;
        for (let i = 0; i <reelList.length; i++) 
        { 
            reelList[i].update(delta);
        }

        
    }

    startAnime()
    {
        let reelList = this.stageElements.children;
        for (let i = 0; i <reelList.length; i++) 
        {
            reelList[i].onStartAnimate();
        }
    }

   onStart()
   {
        this.stageElements.children.forEach(x=>{ x.onStartRotate()});
   } 

   onStop()
   {
        this.stageElements.children.forEach(x=>{ x.onStopRotate()});
        PIXI.sound.play('./res/sound/reelstop.mp3');
   }

    addNextElement(posY)
    { 
        let texture = this.slotMachine.getRandomElementTexture();

        if(this.status === Reel.STATUS_NEED_STOP && 
           this.indexInsertDrop >= this.COUNT_ADD_ELEMENT/2 && 
           this.indexInsertDrop <= this.COUNT_VIEW_ELEMENTS - this.COUNT_ADD_ELEMENT/2 +1)
        { 
            texture = this.elementList[this.valueDrop[this.COUNT_ELEMENTS-1 - this.indexInsertDrop-1]];
        }
        let el = new Element(texture);
        el.position.set(0, posY);
        this.stageElements.addChildAt(el, 0);
        return el;
    }

    getElementAt(y)
    {
        return this.stageElements.children[ this.COUNT_ADD_ELEMENT/2 + y]; 
    }
    // Найдем самый нижний элемент
    _findBottomElement () 
    {
        let el = this.stageElements.children[0];
    
        for (let i = 1; i < this.stageElements.children.length; i++) {
            if (el.y < this.stageElements.children[i].y) {
                el = this.stageElements.children[i];
            }
        }
    
        return el;
    }
 
}

export default Reel;