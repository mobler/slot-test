import {RES} from './const';
import SlotMachine from './SlotMachine';
import SlotData from './SlotData';
import {GameNetwork} from './util';

class Game extends PIXI.Container
{
    constructor()
    {
        super();
        this.slotData = new SlotData();
        this.slotMachine = new SlotMachine();
        this.currentBet = 1;
    }

    init()
    {
        let elements = [PIXI.Texture.from(RES.SPRITE.ICON1), 
                        PIXI.Texture.from(RES.SPRITE.ICON2), 
                        PIXI.Texture.from(RES.SPRITE.ICON3),
                        PIXI.Texture.from(RES.SPRITE.ICON4),
                        PIXI.Texture.from(RES.SPRITE.ICON5),
                        PIXI.Texture.from(RES.SPRITE.ICON6),
                        PIXI.Texture.from(RES.SPRITE.ICON7),
                        PIXI.Texture.from(RES.SPRITE.ICON8),
                        PIXI.Texture.from(RES.SPRITE.ICON9),
                        PIXI.Texture.from(RES.SPRITE.ICON10)]

        this.slotMachine.init(elements, this.slotData);
    }

    create()
    { 
        this.slotMachine.create();
        this.slotMachine.position.set(190, 56);

        let imgBack = new PIXI.Sprite.from(RES.SPRITE.BACKGROUND);
        let btnLeftBet = new PIXI.Sprite.from(RES.SPRITE.BTN_MINUS);
        let btnRigthBet = new PIXI.Sprite.from(RES.SPRITE.BTN_PLUS);
        let btnStartBet = new PIXI.Sprite.from(RES.SPRITE.BTN_START);
        let labelBet = new PIXI.Text(this.currentBet, {fontFamily : 'Arial', fontSize: 65, fill : 0xffffff, align : 'center'})    
 
        btnLeftBet.anchor.set(0.5);
        btnLeftBet.position.set(220, 680);
        btnLeftBet.interactive = true;
        btnLeftBet.buttonMode = true;
        btnLeftBet.on('pointerup', this._leftBtn.bind(this));

        btnRigthBet.anchor.set(0.5);
        btnRigthBet.position.set(400, 680);
        btnRigthBet.interactive = true;
        btnRigthBet.buttonMode = true;
        btnRigthBet.on('pointerup', this._rigthBtn.bind(this));

        btnStartBet.anchor.set(0.5);
        btnStartBet.position.set(1000, 680);
        btnStartBet.interactive = true;
        btnStartBet.buttonMode = true;
        btnStartBet.on('pointerup', this._spin.bind(this));

        labelBet.anchor.set(0.5);
        labelBet.position.set(305, 680);

        this.addChild(imgBack);
        this.addChild(btnLeftBet);
        this.addChild(btnRigthBet);
        this.addChild(btnStartBet);
        this.addChild(labelBet);
        this.addChild(this.slotMachine);

        this.labelBet = labelBet;
        this.btnLeftBet = btnLeftBet;
        this.btnRigthBet = btnRigthBet;
        this.btnStartBet = btnStartBet;
    }

    _leftBtn()
    {
        this.currentBet = Math.max(1, this.currentBet-1);
        this.labelBet.text = this.currentBet;
    }

    _rigthBtn()
    {
        this.currentBet = Math.min(10, this.currentBet+1);
        this.labelBet.text = this.currentBet;
    }

    _spin()
    {
        this.onStartSpin();
        this.slotMachine.start();
        
        this._sendRequest();
        
        
    }

    _sendRequest()
    {
        GameNetwork.getSlotSpin({bet: this.currentBet}).then(
            result => {
                this.slotData.setFrom(result);
               return this.slotMachine.stop();
            }
        ).then(result=>{ 
            this.onStopSpin();
        },
        error =>{
            console.log(error)
            setTimeout(this._sendRequest.bind(this), 1000);
        });
    }

    onStartSpin()
    {
      
    }

    onStopSpin()
    {
       
    }

    update(delta)
    {
        this.slotMachine.update(delta);

        if(!this.slotMachine.isAnimateSlot && !this.slotMachine.isRunning)
        {
            this._setBtnPanelStatus(true)
        }else
        {
            this._setBtnPanelStatus(false)
        }
    }

    _setBtnPanelStatus(value)
    {
        if(value)
        {
            this.btnLeftBet.interactive = true;
            this.btnRigthBet.interactive = true;
            this.btnStartBet.interactive = true;

            this.btnLeftBet.tint = 0xffffff;
            this.btnRigthBet.tint = 0xffffff;
            this.btnStartBet.tint = 0xffffff;
        }else
        {
            this.btnLeftBet.interactive = false;
            this.btnRigthBet.interactive = false;
            this.btnStartBet.interactive = false;

            this.btnLeftBet.tint = 0x777777;
            this.btnRigthBet.tint = 0x777777;
            this.btnStartBet.tint = 0x777777;
        
        }
    }
}

export default Game;