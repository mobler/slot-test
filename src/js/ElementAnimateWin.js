class ElementAnimateWin
{
    constructor(element)
    {
        this.element = element;

        this.timer = 0;
        this.time = 1200;
        this.progress = 0;
    }

    update(delta)
    {
        this.timer += delta;
        this.progress = this.timer / this.time ;
 
    }

    beforeAnimate()
    {
        this.element.alpha = 1;
    }

    updateProgress()
    {
        if(this.element.data.isWin)
            this.element.alpha = Math.abs(Math.cos(this.progress*Math.PI*3));
    }

    afterAnimate()
    {
        this.element.alpha = 1;
    }
 
}

export default ElementAnimateWin;