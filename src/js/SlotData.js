class SlotData
{
    constructor()
    {
        this.COUNT_ROW = 3;
        this.COUNT_COL = 5;
        this.originJSON = {};
        this.bet = 0;
        this.win = 0;
        this.board = [];
        this.winLines = [];
    };

    setFrom(json)
    { 
        this.originJSON = json;

        this.bet = parseInt(json.bet);
        this.win = parseInt(json.win);
        this.board = json.board.split(',');
        this.winLines = this._getWinLines(json.winlines);

        console.log('SERVER DATA:', json);
    }

    // from 0
    // icon from 1..n -> 0..n-1
    getElementFromBord(col, row)
    {
        return parseInt(this.board[col*this.COUNT_ROW+row]) - 1;
    }

    getBoard()
    {
        let list = [];

        for(let i=0; i<this.COUNT_COL; i++)
        {
            list.push(this.getElementsColAt(i));    
        }

        return list;
    }

    // Получить все колонку
    getElementsColAt(col)
    {
        let list = [];

        for(let i=0; i<this.COUNT_ROW; i++)
            list.push(this.getElementFromBord(col, i));

        return list;
    }

    _getWinLines(lines)
    {
        let list = [];
 
        for(let i=0; i<lines.length; i++)
        {
            let arr = lines[i].split('~');
            let index = parseInt(arr[0]);
            let win = parseInt(arr[1]);
            let elementsList = arr[2].split(',');
            let elementsPos = [];
            
            for(let n=0; n<elementsList.length; n++)
            {
                let index = parseInt(elementsList[n]);
                let x = parseInt(index/this.COUNT_ROW);
                let y = index -  x*this.COUNT_ROW;
                var elPos = [x, y];
                elementsPos.push(elPos);
            }
            

            let winLine = {
                index: index,
                win: win,
                elements: elementsList,
                elementsPos: elementsPos 
            }
            
            list.push(winLine)  
        }

        return list;
    }
}

export default SlotData;