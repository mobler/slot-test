var gulp        = require('gulp');
var browserSync = require('browser-sync').create(); 
var connect = require('gulp-connect-php');
var runSequence = require('run-sequence');

const browserify = require('browserify')
const source = require('vinyl-source-stream'); 

// Static server
gulp.task('startServer', function() {
    // connect.server({});
    browserSync.init({
        server: {
           proxy: 'localhost:8000',
           port: 8080,
            baseDir: "./src" 
        }
    });

    gulp.watch(['src/js/**/*']).on('change', ()=>{
        runSequence('build', 'reloadBrowser');
    });  
 
});
 
gulp.task('reloadBrowser', function() {
    browserSync.reload();
}); 

gulp.task('build', function() {
    return browserify('./src/js/main.js')
      .transform('babelify', {
          presets: ['es2015']
      })
      .bundle()	
      .pipe(source('bundle.js'))
      .pipe(gulp.dest('./src/build/'));
  });



gulp.task('default', ()=>
{
    runSequence('build', 'startServer');
});

 